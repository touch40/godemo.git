package main

import "fmt"

func main() {
	// var ints = map[int]int64{1: 1, 2: 2}
	// fmt.Println(SumIntsOrFloats[int, int64](ints))
	// var floats = map[string]float64{"1": 2.5, "2": 2.3}
	// fmt.Println(SumIntsOrFloats[string, float64](floats))
	PrintT[int]([]int{1, 2, 3, 4})
	PrintT[string]([]string{"a", "b", "c", "d"})

}

// SumIntsOrFloats sums the values of map m. It supports both int64 and float64
// as types for map values.
func SumIntsOrFloats[K comparable, V int64 | float64](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}
func PrintT[T int | string](data []T) {
	var td T
	for _, v := range data {
		td += v
		fmt.Printf("%+v\n", td)
	}
}
