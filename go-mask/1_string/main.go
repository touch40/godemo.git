package main

import (
	"fmt"

	"github.com/showa-93/go-mask"
)

func main() {
	maskValue, _ := mask.String(mask.MaskTypeFixed, "Hello World!!")
	fmt.Println(maskValue)
}
