package main

import (
	"log"
	"net"

	"gitee.com/touch40/godemo/grpc/service"
	"google.golang.org/grpc"
)

func main() {
	server := grpc.NewServer()
	service.RegisterProdServiceServer(server, service.ProductService)

	listener, err := net.Listen("tcp", ":8002")
	if err != nil {
		log.Fatal("6服务监听端口失败", err)
	}
	_ = server.Serve(listener)
}
