package main

import (
	"fmt"
	"time"

	"github.com/robfig/cron/v3"
)

func main() {
	// c := cron.New()
	// 增加默认值为秒
	// Seconds field, required
	c := cron.New(cron.WithSeconds())
	// // Seconds field, optional
	// cron.New(cron.WithParser(cron.NewParser(
	// 	cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor,
	// )))

	// i := 0
	// c.AddFunc("@every 1s", func() {
	// 	time.Sleep(time.Second * 5)
	// 	i++
	// 	fmt.Println("Every hour on the half hour " + strconv.Itoa(i))
	// })

	c.AddFunc("3 * * * * *", func() {
		fmt.Println(time.Now())
	})

	c.Start()
	t := time.Tick(time.Second * 1000)
	<-t
}
