package main

import "sync"

type Person struct {
	Age int
}

// 初始化pool
var personPool = sync.Pool{
	New: func() interface{} {
		return new(Person)
	},
}

func main() {
	// 获取一个实例
	newPerson := personPool.Get().(*Person)
	// 回收对象 以备其他协程使用
	defer personPool.Put(newPerson)

	newPerson.Age = 25
}
