package main

import "github.com/sirupsen/logrus"

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	logrus.Trace("1---trace---msg")
	logrus.Debug("2---debug---msg")
	logrus.Info("3---info---msg")
	logrus.Warn("4---warn---msg")
	logrus.Error("5---error---msg")
	logrus.Fatal("6---fatal---msg")
	logrus.Panic("7---panic---msg")
}
