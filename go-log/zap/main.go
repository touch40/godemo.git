package main

import (
	"os"
	"time"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	logger *zap.Logger
	sugar  *zap.SugaredLogger
)

func InitLogger() *zap.Logger {
	encoder := getEncoder()
	writeSyncer := getLogWriter()
	core := zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(writeSyncer, zapcore.AddSync(os.Stdout)), zapcore.DebugLevel)
	logger = zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1), zap.AddStacktrace(zap.WarnLevel)) // zap.Hooks(ZapLogHandler),
	defer logger.Sync()
	sugar = logger.Sugar()
	return logger
}

// getEncoder zapcore.Encoder
func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
	}

	// if global.Config.Log.StacktraceKey != "" {
	// 	encoderConfig.StacktraceKey = global.Config.Log.StacktraceKey
	// }

	// return zapcore.NewJSONEncoder(encoderConfig)

	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogWriter() zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./data/logs/touchflow.log", // 日志文件位置
		MaxSize:    1024,                        // 进行切割之前,日志文件的最大大小(MB为单位)
		MaxBackups: 100,                         // 保留旧文件的最大个数
		MaxAge:     100,                         // 保留旧文件的最大天数
		Compress:   false,                       // 是否压缩/归档旧文件
	}

	return zapcore.AddSync(lumberJackLogger)
}

func ZapLogHandler(entry zapcore.Entry) error {
	// 参数 entry 介绍
	// entry  参数就是单条日志结构体，主要包括字段如下：
	//Level      日志等级
	//Time       当前时间
	//LoggerName  日志名称
	//Message    日志内容
	//Caller     各个文件调用路径
	//Stack      代码调用栈

	//这里启动一个协程，hook丝毫不会影响程序性能，
	go func(paramEntry zapcore.Entry) {
		//fmt.Println(" GoSkeleton  hook ....，你可以在这里继续处理系统日志....")
		//fmt.Printf("%#+v\n", paramEntry)
	}(entry)
	return nil
}

func main() {
	L := InitLogger()
	L.Info("msg", zap.String("a", "b"))
	// fmt.Println("aaab")
}
