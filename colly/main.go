package main

import (
	"fmt"
	"log"

	"github.com/gocolly/colly/v2"
)

func main() {
	// 创建收集器
	c := colly.NewCollector()

	// 匹配指定元素后回调
	c.OnHTML("#su", func(e *colly.HTMLElement) {
		baiduBtn := e.Attr("value")
		fmt.Println("匹配到目标元素ID su:", baiduBtn)
	})

	// 可以多次 OnHTML 不同的页面元素
	c.OnHTML("#kw", func(e *colly.HTMLElement) {
		maxlength := e.Attr("maxlength")
		fmt.Println("匹配到目标元素ID kw, 最多允许输入:", maxlength)
	})

	// 请求发起时回调,一般用来设置请求头等
	c.OnRequest(func(request *colly.Request) {
		fmt.Println("----> 开始请求了")
	})

	// 请求完成后回调
	c.OnResponse(func(response *colly.Response) {
		fmt.Println("----> 开始返回了")
	})

	//请求发生错误回调
	c.OnError(func(response *colly.Response, err error) {
		fmt.Printf("发生错误了:%v", err)
	})

	// 在所有OnHTML之后执行，可以用来做一些回收操作
	c.OnScraped(func(response *colly.Response) {
		fmt.Println("----> 所有匹配已完成")
	})

	// 和OnHTML类似，用于匹配xpath解析
	// 匹配：百度首页左上方链接
	c.OnXML("//div[@id='s-top-left']/a", func(element *colly.XMLElement) {
		text := element.Text
		href := element.Attr("href")
		fmt.Printf("名称:%s -> 连接:%s\n", text, href)
	})

	// 开始访问
	err := c.Visit("http://www.baidu.com/")
	if err != nil {
		log.Fatalln(err)
		return
	}
}
