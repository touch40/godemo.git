package main

import (
	"fmt"
	"strings"
)

func main() {

	c := strings.TrimPrefix("001", "0")
	fmt.Println(c)
	// a := []int{1, 34, 52, 11, 22, 34, 34, 12, 11, 5}
	// quicksort(a)
	// fmt.Printf("%v", a)
}

func quicksort(nums []int) {
	sort1(nums, 0, len(nums)-1)
}

func sort1(nums []int, i, j int) {
	if i >= j {
		return
	}
	ni := change(nums, i, j)
	sort1(nums, i, ni-1)
	sort1(nums, ni+1, j)
}

func change(nums []int, i, j int) int {
	l, r := i, j
	p := nums[i]
	for l < r {
		for l < r && nums[r] >= p {
			r--
		}
		if l >= r {
			break
		}
		nums[l] = nums[r]
		l++
		for l < r && nums[l] <= p {
			l++
		}
		if l >= r {
			break
		}
		nums[r] = nums[l]
		r--
	}
	nums[l] = p
	return l
}
