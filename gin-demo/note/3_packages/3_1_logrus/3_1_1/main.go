package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var log = logrus.New()

func initLogrus() error {
	log.Formatter = &logrus.JSONFormatter{}                                              // 设置为接送格式的日志
	file, err := os.OpenFile("./gin_log.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644) //自动创建|可读写|追加,权限
	if err != nil {
		fmt.Println("创建文件、打开文件失败！")
		return err
	}
	log.Out = file
	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = log.Out
	log.Level = logrus.InfoLevel
	return nil
}

func main() {
	err := initLogrus()
	if err != nil {
		fmt.Println(err)
		return
	}
	r := gin.Default()
	r.GET("/logrus", func(c *gin.Context) {
		log.WithFields(logrus.Fields{
			"url":    c.Request.RequestURI,
			"method": c.Request.Method,
			"params": c.Query("name"),
			"IP":     c.ClientIP(),
		}).Info()
		resData := struct {
			Code int    `json:"code"`
			Msg  string `json:"msg"`
			Data any    `json:"data"`
		}{http.StatusOK, "响应成功", "OK"}
		c.JSON(http.StatusOK, resData)
	})
	r.Run(":9090")
}
