package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

var (
	sqlDb       *sql.DB
	sqlResponse SqlResponse
)

func init() {
	// 打开数据库
	sqlStr := "root:touch@40@tcp(127.0.0.1:3306)/testdb?charset=utf8&parseTime=true&loc=Local"
	var err error
	sqlDb, err = sql.Open("mysql", sqlStr)
	if err != nil {
		fmt.Println("数据库打开失败：", err)
		return
	}
	err = sqlDb.Ping()
	if err != nil {
		fmt.Println("数据库连接失败：", err)
		return
	}
}

// Client提交的数据
type SqlUser struct {
	Name    string `json:"name"`
	Age     int    `json:"age"`
	Address string `json:"address"`
}

type SqlResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

func insertData(c *gin.Context) {
	var u SqlUser
	err := c.Bind(&u)
	if err != nil {
		sqlResponse.Code = http.StatusBadRequest
		sqlResponse.Message = "参数错误"
		sqlResponse.Data = "error"
		c.JSON(http.StatusBadRequest, sqlResponse)
		return
	}
	sqlStr := "insert into user(name,age,address) values(?,?,?)"
	ret, err := sqlDb.Exec(sqlStr, u.Name, u.Age, u.Address)
	if err != nil {
		sqlResponse.Code = http.StatusBadRequest
		sqlResponse.Message = "写入失败"
		sqlResponse.Data = "error"
		c.JSON(http.StatusBadRequest, sqlResponse)
		return
	}
	sqlResponse.Code = http.StatusBadRequest
	sqlResponse.Message = "写入成功"
	sqlResponse.Data = "ok"
	c.JSON(http.StatusBadRequest, sqlResponse)
	fmt.Println(ret.LastInsertId())
}

func main() {
	r := gin.Default()
	r.POST("sql/insert", insertData)
	r.Run(":9090")
}
