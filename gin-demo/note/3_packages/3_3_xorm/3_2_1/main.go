package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
)

var x *xorm.Engine

// 定义结构体(xorm支持双向映射)：没有表，会进行创建
type Stu struct {
	Id      int64     `xorm:"pk autoincr" json:"id"`
	StuNum  string    `xorm:"unique" json:"stu_num"`
	Name    string    `json:"name"`
	Age     int       `json:"age"`
	Created time.Time `xorm:"created" json:"created"`
	Updated time.Time `xorm:"updated" json:"updated"`
}

// 应答体
type XormResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

var xormResponse XormResponse

func init() {
	sqlStr := "root:touch@40@tcp(127.0.0.1:3306)/testdb?charset=utf8&parseTime=true&loc=Local"
	var err error
	x, err = xorm.NewEngine("mysql", sqlStr)
	if err != nil {
		fmt.Println("数据库连接失败：", err)
	}
	//2.创建或者同步表（名称为Stu）
	err = x.Sync(new(Stu))
	if err != nil {
		fmt.Println("同步失败：", err)
	}
}

func main() {
	r := gin.Default()
	// 数据的CURD acid
	o := r.Group("orm")
	{
		o.POST("insert", xormInsertData)
	}
	r.Run(":9090")
}

func xormInsertData(c *gin.Context) {
	var s Stu
	err := c.Bind(&s)
	if err != nil {
		xormResponse.Code = http.StatusBadRequest
		xormResponse.Message = "参数错误"
		xormResponse.Data = "error"
		c.JSON(http.StatusOK, xormResponse)
		return
	}
	affected, err := x.Insert(s)
	if err != nil || affected <= 0 {
		xormResponse.Code = http.StatusBadRequest
		xormResponse.Message = "参数错误"
		xormResponse.Data = "error"
		c.JSON(http.StatusOK, xormResponse)
		return
	}
	xormResponse.Code = http.StatusOK
	xormResponse.Message = "写入成功"
	xormResponse.Data = "OK"
	c.JSON(http.StatusOK, xormResponse)
	fmt.Println(affected)
}
