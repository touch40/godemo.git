package main

import (
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	jwt "github.com/golang-jwt/jwt/v4"
)

type HmacUser struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Telephone string `json:"telephone"`
	Password  string `json:"password"`
}

type MyClaims struct {
	UserId string
	jwt.StandardClaims
}

var jwtKey = []byte("a_secret_key") // 证书签名秘钥（该秘钥非常重要，如果Client端有该秘钥，就可以分发证书了）
func main() {
	r := gin.Default()
	r.POST("getToken1", func(c *gin.Context) {
		var u HmacUser
		err := c.Bind(&u)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, "参数错误！")
		}
		token, err := hmacReleaseToken(u)
		if err != nil {
			c.JSON(http.StatusInternalServerError, err)
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"msg":  "token分发成功",
			"data": token,
		})
	})
	// token的认证
	r.POST("/checkToken1", hmacAuthMiddleware(), func(c *gin.Context) {
		c.JSON(http.StatusOK, "验证成功")
	})
	r.Run(":9090")
}

func hmacAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := "zkx"
		// 获取对应token
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" || !strings.HasPrefix(tokenString, auth+":") {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "前缀错误"})
			c.Abort()
			return
		}
		index := strings.Index(tokenString, auth+":") // 找到token对应的位置
		// 真实的token的值
		tokenString = tokenString[index+len(auth)+1:]
		token, claims, err := hamcParseToken(tokenString)
		if err != nil || !token.Valid {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "证书无效"})
			c.Abort()
			return
		}
		var u HmacUser
		c.Bind(&u)
		if u.Id != claims.UserId {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "用户不存在"})
			c.Abort()
			return
		}
		c.Next()
	}
}

// 解析token
func hamcParseToken(tokenString string) (*jwt.Token, *MyClaims, error) {
	claims := MyClaims{}
	token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (i interface{}, err error) {
		return jwtKey, nil
	})
	return token, &claims, err
}

// 分发token
func hmacReleaseToken(u HmacUser) (string, error) {
	expirationTime := time.Now().Add(7 * 24 * time.Hour)
	claims := MyClaims{
		UserId: u.Id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(), // 过期时间
			IssuedAt:  time.Now().Unix(),     // 发布时间
			Subject:   "User token",          // 主题
			Issuer:    "zkx",                 // 发布者
		}}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, err
}
