package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/file", fileServer)
	r.Run(":9090")
}

func fileServer(c *gin.Context) {
	path := `D:\go\src\gitee.com\touch40\godemo\gin-demo\note\2_reponse\2_4_fileserver\`
	fileName := path + c.Query("name")
	c.File(fileName)
}
