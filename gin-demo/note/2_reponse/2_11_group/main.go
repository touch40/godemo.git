package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type ResGroup struct {
	Data string `json"data"`
	Path string `json:"path"`
}

func main() {
	router := gin.Default()

	v1 := router.Group("/v1")
	{
		r := v1.Group("/user")
		{
			r.GET("/login", login)
		}
	}
	router.Run(":9090")
}

func login(c *gin.Context) {
	c.JSON(http.StatusOK, ResGroup{"login", c.Request.URL.Path})
}
