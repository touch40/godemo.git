package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Login struct {
	UserName string `json:"user_name" binding:"required"`
	Password string `json:"password" binding:"required"`
	Remark   string `json:"remark"`
}

func main() {
	r := gin.Default()
	r.POST("/login", func(c *gin.Context) {
		var login Login
		err := c.Bind(&login)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"msg":  "绑定失败",
				"data": err.Error(),
			})
			return
		}
		if login.UserName == "user" && login.Password == "123456" {
			c.JSON(http.StatusOK, gin.H{
				"msg":  "登陆成功",
				"data": "OK",
			})
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":  "登陆失败",
			"data": "error",
		})
	})
	r.Run(":9090")
}
