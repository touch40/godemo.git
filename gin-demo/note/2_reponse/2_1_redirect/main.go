package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default() // 路由引擎
	r.GET("/redirect1", func(c *gin.Context) {
		url := "http://www.baidu.com"
		c.Redirect(http.StatusMovedPermanently, url)
	})

	r.GET("/redirect2", func(c *gin.Context) {
		c.Request.URL.Path = "/TestRedirect"
		r.HandleContext(c)
	})
	r.GET("/TestRedirect", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"msg":  "响应成功",
		})
	})
	r.Run(":9090")
}
