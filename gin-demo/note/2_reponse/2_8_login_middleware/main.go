package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	// 路由使用gin.BasicAuth() 中间件
	r.Use(AuthMiddleware())
	r.GET("/login", func(c *gin.Context) {
		user := c.MustGet(gin.AuthUserKey).(string)
		c.JSON(http.StatusOK, user+"登陆成功")
	})
	// 监听并在0.0.0.0:9090 上启动服务
	r.Run(":9090")
}

func AuthMiddleware() gin.HandlerFunc {
	// 初始化用户
	accounts := gin.Accounts{
		"admin":  "adminpw",
		"system": "systempw",
	}
	accounts["go"] = "123123"
	accounts["gin"] = "123123"
	auth := gin.BasicAuth(accounts)
	return auth
}
