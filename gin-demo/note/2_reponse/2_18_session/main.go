package main

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

var (
	sessionName  string
	sessionValue string
)

type MyOption struct {
	sessions.Options
}

func main() {
	r := gin.Default()
	store := cookie.NewStore([]byte("session_secret"))
	r.Use(sessions.Sessions("mysession", store))
	r.GET("/session", func(c *gin.Context) {
		name := c.Query("name")
		if len(name) <= 0 {
			c.JSON(http.StatusBadRequest, "数据错误")
			return
		}
		sessionName = "session_" + name
		sessionValue = "session_value_" + name
		session := sessions.Default(c)
		sessionData := session.Get(sessionName)
		if sessionData != sessionValue {
			// c.String(http.StatusOK, "Cookie:%s已经下发", sessionName)
			session.Set(sessionName, sessionValue)
			o := MyOption{}
			o.Path = "/"
			o.MaxAge = 10
			session.Options(o.Options)
			session.Save()
			c.JSON(http.StatusOK, "首次访问,session已保存")
			return
		}
		c.String(http.StatusOK, "访问成功，session值为:%s", sessionData.(string))
	})
	r.Run(":9090")
}
