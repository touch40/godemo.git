package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// 跨域
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method               //请求方法
		origin := c.Request.Header.Get("Origin") //请求头部
		var headerKeys []string                  // 声明请求头keys
		for k := range c.Request.Header {
			headerKeys = append(headerKeys, k)
		}
		headerStr := strings.Join(headerKeys, ", ")
		if headerStr != "" {
			headerStr = fmt.Sprintf("access-control-allow-origin, access-control-allow-headers, %s", headerStr)
		} else {
			headerStr = "access-control-allow-origin, access-control-allow-headers"
		}
		if origin != "" {
			c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Origin", "*")
			// 这是允许访问所有域
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE,UPDATE")
			//服务器支持的所有跨域请求的方法,为了避免浏览次请求的多次'预检'请求
			//  header的类型
			c.Header("Access-Control-Allow-Headers", "Authorization, Content-Length, X-CSRF-Token, Token,"+
				"session,X_Requested_With,Accept, Origin, Host, Connection, Accept-Encoding, Accept-Language,DNT, "+
				"X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, "+
				"Content-Type, Pragma")
			// 允许跨域设置
			// 可以返回其他子段
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, "+
				"Access-Control-Allow-Headers,Cache-Control,Content-Language,Content-Type,Expires,Last-Modified,Pragma,FooBar")
			// 跨域关键设置 让浏览器可以解析
			c.Header("Access-Control-Max-Age", "172800")
			// 缓存请求信息 单位为秒
			c.Header("Access-Control-Allow-Credentials", "false")
			//  跨域请求是否需要带cookie信息 默认设置为true
			c.Set("content-type", "application/json")
			// 设置返回格式是json
		}
		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.JSON(http.StatusOK, "Options Request!")
		}
		// 处理请求
		c.Next() //  处理请求
	}
}

// 跨域中间件
// (跨域，指的是浏览器不能执行其它网站的脚本)
// 它是由浏览器的同源策略造成的，是浏览器对JavaScript施加的安全限制。）
func CORSMiddleware1() gin.HandlerFunc { // CORS是跨源资源分享（Cross-Origin Resource Sharing）中间件
	return func(ctx *gin.Context) {
		// 指定允许其它域名访问
		// ctx.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		// 预检结果缓存时间
		ctx.Writer.Header().Set("Access-Control-Max-Age", "86400")
		// 允许的请求类型（get，post等）
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "*")
		// 允许的请求头字段
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		// 是否允许后续请求携带认证信息（cookies），该值只能是true，否则不返回
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		if ctx.Request.Method == http.MethodOptions {
			ctx.AbortWithStatus(http.StatusOK)
		} else {
			ctx.Next()
		}

	}
}
