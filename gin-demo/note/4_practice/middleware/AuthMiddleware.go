package middleware

import (
	"net/http"
	"strings"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/model"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/common"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := "zkx"
		// 获取对应token
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" || !strings.HasPrefix(tokenString, auth+":") {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "前缀错误"})
			c.Abort()
			return
		}
		index := strings.Index(tokenString, auth+":") // 找到token对应的位置
		// 真实的token的值
		tokenString = tokenString[index+len(auth)+1:]
		token, claims, err := common.ParseToken(tokenString)
		if err != nil || !token.Valid {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "证书无效"})
			c.Abort()
			return
		}
		var user model.User
		common.DB.First(&user, claims.Id)
		if user.ID == 0 {
			c.JSON(http.StatusUnauthorized, gin.H{"code": http.StatusUnauthorized, "msg": "用户不存在"})
			c.Abort()
			return
		}
		c.Set("user", user) // 将key-value值存储到context中
		c.Next()
	}
}
