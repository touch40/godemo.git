package controller

import (
	"net/http"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/common"

	"gitee.com/touch40/godemo/gindemo/note/4_practice/model"
	"gitee.com/touch40/godemo/gindemo/note/4_practice/response"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func Register(ctx *gin.Context) {
	var requestUser model.User
	ctx.Bind(&requestUser)
	name := requestUser.Name
	telephone := requestUser.Telephone
	password := requestUser.Password
	// 数据校验
	if len(telephone) != 11 {
		response.Response(ctx, http.StatusUnprocessableEntity, 400, nil, "手机号必须填写")
		return
	}
	if len(password) < 6 {
		response.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "密码不能少于6位")
		return
	}

	if len(name) == 0 {
		response.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "用户名不能为空")
		return
	}
	if isTelephoneExist(common.DB, telephone) {
		response.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "手机号重复")
		return
	}
	// 创建用户
	newUser := model.User{
		Name:      name,
		Telephone: telephone,
	}

	common.DB.Create(&newUser)
	token, err := common.ReleaseToken(newUser)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "系统异常"})
		return
	}
	response.Success(ctx, gin.H{
		"token": token,
	}, "注册成功")
}

func isTelephoneExist(db *gorm.DB, telephone string) bool {
	var user model.User
	db.Where("telephone=?", telephone).First(&user)
	return user.ID == 0
}

func Login(ctx *gin.Context) {
	var requestUser model.User
	ctx.Bind(&requestUser)
	name := requestUser.Name
	telephone := requestUser.Telephone
	password := requestUser.Password
	// 数据验证
	if len(password) < 6 {
		response.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "密码不能少于6位")
		return
	}

	// 依据手机号,查询用户注册的数据记录
}
