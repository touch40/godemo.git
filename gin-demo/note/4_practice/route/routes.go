package route

import (
	"gitee.com/touch40/godemo/gindemo/note/4_practice/middleware"
	"github.com/gin-gonic/gin"
)

func CollectRoute(r *gin.Engine) *gin.Engine {
	r.Use(gin.Recovery())
	r.Use(middleware.CORSMiddleware())
	api := r.Group("api")
	{
		auth := api.Group("/api/auth")
		{
			auth.POST("/register")
			auth.POST("/login")
			auth.GET("/info")
		}
	}
	return r
}
