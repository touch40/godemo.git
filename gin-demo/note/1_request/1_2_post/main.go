package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()       // 路由引擎
	r.POST("/post", postMsg) // get方法
	r.Run(":9090")
}

func postMsg(c *gin.Context) {
	// name := c.Query("name")// 获取URL中的数据
	name := c.DefaultPostForm("name", "hello")
	// c.String(http.StatusOK, "欢迎您：%s", name)
	form, b := c.GetPostForm("name")
	fmt.Println(form, b)
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"msg":  "返回信息",
		"data": "欢迎您" + name,
	})
}
