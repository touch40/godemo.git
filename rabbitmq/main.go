package main

import (
	"log"

	"github.com/streadway/amqp"
)

func main() {
	// 建立连接
	uri := "amqp://quest:quest@127.0.0.1:5672/"
	connection, err := amqp.Dial(uri)
	if err != nil {
		log.Fatal(err)
	}
	defer connection.Close()
	channel, err := connection.Channel()
	if err != nil {
		log.Fatal(err)
	}
	defer channel.Close()

}
