package main

import (
	"fmt"
	"strings"
	"unicode"
)

func main() {
	fmt.Println(unicode.IsDigit(rune('1')))
	fmt.Println(unicode.IsLetter(rune('1')))
	fmt.Println(unicode.IsGraphic(rune('$')))
	fmt.Println(unicode.ToLower(rune('1')))
	fmt.Println(strings.ToUpper("cMccEdu"))
}
