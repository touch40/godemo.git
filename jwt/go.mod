module gitee.com/touch40/godemo/jwt

go 1.18

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofrs/uuid v4.4.0+incompatible
)
