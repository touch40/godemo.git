```
docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=“123456”  -v /usr/local/docker_data/mysql/data:/var/lib/mysql -v /usr/local/docker_data/mysql/conf:/etc/mysql/ -v /usr/local/docker_data/mysql/logs:/var/log/mysql mysql:5.7


```

参数	说明
–name mysql5.7	容器名称
-p 3306:3306	端口映射(宿主机端口:容器端口)
-e MYSQL_ROOT_PASSWORD=123456	容器的环境变量(root账号初始化密码)
-d	后台运行容器
-v /usr/local/docker_data/mysql/data:/var/lib/mysql	容器MySQL数据目录映射(宿主机:容器)
-v /usr/local/docker_data/mysql/conf:/etc/mysql/	容器MySQL配置目录映射(宿主机:容器)
-v /usr/local/docker_data/mysql/logs:/var/log/mysql	容器MySQL日志目录映射(宿主机:容器)
mysql:5.7	指定docker镜像 (可以是镜像名称或者镜像ID)