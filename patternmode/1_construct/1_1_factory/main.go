package main

func main() {
	// apple := NewFruit("apple")
	// apple.Show("apple")

	// banana := NewFruit("banana")
	// banana.Show("banana")

	// pear := NewFruit("pear")
	// pear.Show("pear")

	// factory := new(Factory)

	// apple := factory.CreateFruit("apple")
	// apple.Show()

	// banana := factory.CreateFruit("banana")
	// banana.Show()

	// pear := factory.CreateFruit("pear")
	// pear.Show()
	var appleFac AbstractFactory = new(AppleFactory)
	apple := appleFac.CreateFruit()
	apple.Show()
}
